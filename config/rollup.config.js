import resolve from 'rollup-plugin-node-resolve';
import commonJS from 'rollup-plugin-commonjs';

const globby = require('globby');

const baseConfigs = globby.sync('*.js').filter(inputFile => !inputFile.includes('test')).map(inputFile => ({
  name: inputFile.substr((inputFile.lastIndexOf('/') || 0)),
  entry: inputFile,
  dest: `dist/${inputFile}`,
  format: 'es',
  plugins: [
    resolve(),
    commonJS({
      include: 'node_modules/**',
    }),
  ],
}));

const umdConfigs = globby.sync('*.js').filter(inputFile => !inputFile.includes('test')).map(inputFile => ({
  name: inputFile.substr((inputFile.lastIndexOf('/') || 0)),
  entry: inputFile,
  dest: `dist/${inputFile.replace('.js', '.umd.js')}`,
  format: 'umd',
  plugins: [
    resolve(),
    commonJS({
      include: 'node_modules/**',
    }),
  ],
}));

const iifeConfigs = globby.sync('*.js').filter(inputFile => !inputFile.includes('test')).map(inputFile => ({
  name: inputFile.substr((inputFile.lastIndexOf('/') || 0)),
  entry: inputFile,
  dest: `dist/${inputFile.replace('.js', '.iife.js')}`,
  format: 'iife',
  plugins: [
    resolve(),
    commonJS({
      include: 'node_modules/**',
    }),
  ],
}));

const cjsConfigs = globby.sync('*.js').filter(inputFile => !inputFile.includes('test')).map(inputFile => ({
  name: inputFile.substr((inputFile.lastIndexOf('/') || 0)),
  entry: inputFile,
  dest: `dist/${inputFile.replace('.js', '.cjs.js')}`,
  format: 'cjs',
  plugins: [
    resolve(),
    commonJS({
      include: 'node_modules/**',
    }),
  ],
}));

export default [
  ...baseConfigs,
  ...umdConfigs,
  ...iifeConfigs,
  ...cjsConfigs,
];