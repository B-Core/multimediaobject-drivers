var getNumFromString = (str) => {
  if (typeof str === 'string') {
    const num = str.match(/-(?=\d)|\d+|\.\d+/g);
    return num !== null
            ? parseFloat(num.join(''))
            : 0;
  }
  return typeof parseFloat(str) === 'number' && !isNaN(parseFloat(str))
        ? parseFloat(str)
        : 0;
};

const reg = new RegExp(/%|px|vh|vw|em|deg/, 'g');

var getUnitFromString = (str) => {
  let unit = '';
  if (typeof str === 'string') {
    const u = str.match(reg);
    unit = u !== null
            ? u[0]
            : '';
  }
  return unit;
};

var uuid = () => {
  let d = new Date().getTime();
  if (window.performance && typeof window.performance.now === 'function') {
    d += performance.now(); // use high-precision timer if available
  }
  const uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
    const r = (d + Math.random() * 16) % 16 | 0;
    d = Math.floor(d / 16);
    return (c === 'x'
            ? r
            : (r & 0x3 | 0x8)).toString(16);
  });
  return uuid;
};

const COMMENT_REG_EXP = new RegExp(/\n+|(\/\*\*\/\n)+/, 'g');
const NEW_LINE_REG_EXP = new RegExp(/^(\n+|\t+|\t\n+)(?!\w)$/, 'gm');
const BACKTICK_REG_EXP = new RegExp(/`/, 'gm');

const serialize = (funct) => {
  if (!(funct instanceof Function)) throw new TypeError('serialize: argument should be a function');
  const txt = funct.toString();
  const args = txt.slice(txt.indexOf('(') + 1, txt.indexOf(')')).split(',');
  const body = txt.slice(txt.indexOf('{') + 1, txt.lastIndexOf('}'));
  return {
    args: args.map(el => el.replace(COMMENT_REG_EXP, '')
      .replace(NEW_LINE_REG_EXP, '')
      .replace(BACKTICK_REG_EXP, '')),
    body,
  };
};

const COMMENT_REG_EXP$1 = new RegExp(/\n+|(\/\*\*\/\n)+/, 'g');
const NEW_LINE_REG_EXP$1 = new RegExp(/^(\n+|\t+|\t\n+)(?!\w)$/, 'gm');
const BACKTICK_REG_EXP$1 = new RegExp(/`/, 'gm');
const COMMENTS_AND_BACKTICK = new RegExp(/(\/\*``\*\/)+/, 'g');

var unserialize = (serialized) => {
  const args = serialized.args.map(el => el.replace(COMMENT_REG_EXP$1, '').replace(COMMENTS_AND_BACKTICK, '').replace(NEW_LINE_REG_EXP$1, '').replace(BACKTICK_REG_EXP$1, ''));
  const { body } = serialized;
  let func = new Function();
  try {
    func = new Function(args, body);
  } catch (e) {
    console.error(e);
  }
  return func;
};

var difference = (source, inputs) => {
  const flatArray = inputs.filter(i => (i !== null && typeof i !== 'undefined')).map(i => i instanceof Object ? JSON.stringify(i) : i);
  let diffs = source
  .filter(i => (i !== null && typeof i !== 'undefined'))
  .filter((i) => {
    const e = i instanceof Object ? JSON.stringify(i) : i;
    const index = flatArray.indexOf(e);
    if (index >= 0) flatArray.splice(index, 1);
    return !(index >= 0);
  });
  return diffs;
};

const nRegEx = new RegExp(/[\-\+]{0,1}\d+(\.\d+)?/, 'g');
const maxRegEx = new RegExp(/^>\d+(\.\d+)?$/);
const minRegEx = new RegExp(/^<\d+(\.\d+)?$/);
const rangeRegEx = new RegExp(/^\d+(\.\d+)?<>\d+(\.\d+)?$/);

const isNotNativeFunction = (input) => {
  return !input.toString().includes('function Function() { [native code] }')
        && input !== Array
        && input !== Function
        && input !== Object
        && input !== String
        && input !== Number;
};

var index = ({
  validate(input, schema, strict = false) {
    const keys = Object.keys(schema);
    const notVarKeys = keys.filter(k => !k.includes('?'));
    const inputKeys = Object.keys(input);
    const keyDifference = difference(notVarKeys, inputKeys);

    if (keyDifference.length > 0) return this.logError({
      input,
      source: keys,
      valid: false,
      message: `ValidationError: ${JSON.stringify(input)} missing keys [${keyDifference}]`
    });

    inputKeys.forEach(key => {
      const source = (schema[key] || schema[`?${key}`]);
      if (source) this.check(input[key], source);
      else if (!source && strict) return this.checkArray(inputKeys, keys);
    });

    return null;
  },
  check(input, source) {
    if (typeof source !== 'undefined' && source !== null) {
      if (source instanceof Function) {
        if (isNotNativeFunction(source)) {
          const result = source(input);
          return this.logError({
            input,
            source,
            valid: result,
            message: `ValidationError: ${JSON.stringify(input)} did not pass ${source.toString()}: returned ${result}`,
          });
        }
        return this.checkType(input, source);
      }
      const numberTest = (maxRegEx.test(source) || minRegEx.test(source) || rangeRegEx.test(source)) || this.getType(source) === 'Number';
      const stringTest = typeof source === 'string' || source instanceof RegExp || this.getType(source) === 'String';
      const arrayTest = source instanceof Array;

      if (numberTest) {
        return this.checkNumber(input, source);
      } else if (stringTest) {
        return this.checkString(input, source);
      } else if (arrayTest) {
        const typeArray = source.filter(o => o instanceof Function);
        if (typeArray.length > 0 && typeArray.length === source.length) {
          let success = false;
          for (let i = 0; i < typeArray.length; i++) {
            try {
              success = true;
              this.checkType(input, typeArray[i]);
              break;
            } catch (e) {
              success = false;
            }
          }
          if (!success) {
            return this.logError({
              input,
              source,
              valid: false,
              message: `${input} should be of type ${typeArray.map(a => this.getType(a))}`
            });
          }
          return null;
        } else {
          return this.checkArray(input, source);
        }
      } else {
        return this.checkObject(input, source);
      }
    }
    return null;
  },
  checkType(input, source) {
    const t = input ? `${(this.getType(source)[0]).toLowerCase()}${this.getType(source).slice(1)}` : true;
    const tInput = input ? `${(input.constructor.name[0]).toLowerCase()}${input.constructor.name.slice(1)}` : false;
    if (t !== tInput) {
      return this.logError({
        input,
        source,
        valid: false,
        message: `${JSON.stringify(input)} should be of type ${this.getType(source)}`
      });
    }
    return null;
  },
  checkString(input, source) {
    if (source instanceof RegExp) {
      return this.logError({
        input,
        source,
        valid: source.test(input),
      });
    } else {
      return this.logError({
        input,
        source,
        valid: source === input,
      });
    }
  },
  checkNumber(input, source) {
    const minTest = minRegEx.test(source);
    const maxTest = maxRegEx.test(source);
    const rangeTest = rangeRegEx.test(source);
    if (rangeTest && !minTest && !maxTest) {
      const matchMin = source.match(nRegEx)[0];
      const min = parseFloat(matchMin);
      const matchMax = source.match(nRegEx)[1];
      const max = parseFloat(matchMax);
      return this.logError({
        input,
        source,
        valid: input < max && input > min,
      });
    } else if (maxTest && !minTest && !rangeTest) {
      const match = source.match(nRegEx)[0];
      const n = parseFloat(match);
      return this.logError({
        input,
        source,
        valid: input > n,
      });
    } else {
      const match = source.match(nRegEx)[0];
      const n = parseFloat(match);
      return this.logError({
        input,
        source,
        valid: input !== '' && typeof input !== 'undefined' && input !== null && input < n,
      });
    }
  },
  checkArray(input, source) {
    const diffs = difference(source, input);
    return this.logError({
      input: JSON.stringify(input),
      source: JSON.stringify(source),
      valid: diffs.length === 0,
    });
  },
  checkObject(input, source) {
    return this.validate(input, source);
  },
  logError(error) {
    const message = `ValidationError: ${error.input} type:${typeof error.input} must respect ${error.source} type:${typeof error.source}`;
    if (!error.valid) {
      throw new TypeError(error.message || message);
    }
    return null;
  },
  getType(source) {
    return source.name || typeof source;
  }
});

const transformProperties = [
  'translate',
  'translateX',
  'translateY',
  'translateZ',
  'translateX',
  'translate-x',
  'translateY',
  'translate-y',
  'translate-z',
  'scale',
  'scaleX',
  'scaleY',
  'scaleZ',
  'scale-x',
  'scale-y',
  'scale-z',
  'rotate',
  'rotateX',
  'rotateY',
  'rotateZ',
  'rotate-x',
  'rotate-y',
  'rotate-z',
  'skew',
  'skewX',
  'skewY',
  'skewZ',
  'skew-x',
  'skew-y',
  'skew-z',
];

/**
 * transform a trasnform value (in CSS) in a suitable form
 * to be treated afterward.
 * 
 * @param {string} k 
 * @param {string | Number} v
 * 
 * @return Object{string, string, number} - {string, unit, value} 
 */
const transformValueForProperty = (k, v) => {
  let value = 0;
  let unit;
  value = typeof v === 'string' && v.indexOf(',') >= 0
        ? v.split(',').map(getNumFromString)
        : getNumFromString(v);
  unit = typeof v === 'string' && v.indexOf(',') >= 0
          ? v.split(',').map(getUnitFromString)
          : getUnitFromString(v);

  let string = `${k}(${value}${unit})`;
  if (value instanceof Array && unit instanceof Array) {
    let res = '';
    value.forEach((val, index$$1) => {
      res += index$$1 > 0
                ? `, ${val}${unit[index$$1]}`
                : `${val}${unit[index$$1]}`;
    });
    string = `${k}(${res})`;
  }
  return { string, unit, value };
};


/**
 * Transform the 3d transform properties in a CSS driver namespace.
 * Add a 'transform' propertie to the namespace that represents the correctly
 * ordonnanced properties.
 * 
 * @param {Object} namespace - appropriate namespace for the driver
 */
const applyTransformProperties = (namespace) => {
  const transforms = Object.keys(namespace.style).filter(k => transformProperties.includes(k))
                    .reduce((acc, k) => {
                      acc.push([k, namespace.style[k]]);
                      return acc;
                    }, []);
  let z = [0, 1, 2, 3];
  const trans = {
    x: namespace.style.translateX ? getNumFromString(namespace.style.translateX) : 0,
    y: namespace.style.translateY ? getNumFromString(namespace.style.translateY) : 0,
    z: namespace.style.translateZ ? getNumFromString(namespace.style.translateZ) : 0,
    xU: namespace.style.translateX ? getUnitFromString(namespace.style.translateX) : 'px',
    yU: namespace.style.translateY ? getUnitFromString(namespace.style.translateY) : 'px',
    zU: namespace.style.translateZ ? getUnitFromString(namespace.style.translateZ) : 'px',
  };
  const rot = {
    x: namespace.style.rotateX ? getNumFromString(namespace.style.rotateX) : 0,
    y: namespace.style.rotateY ? getNumFromString(namespace.style.rotateY) : 0,
    z: namespace.style.rotateZ ? getNumFromString(namespace.style.rotateZ) : 0,
    u: 'deg',
  };
  const ske = {
    x: namespace.style.skewX ? getNumFromString(namespace.style.skewX) : 0,
    y: namespace.style.skewY ? getNumFromString(namespace.style.skewY) : 0,
    u: 'deg',
  };
  const sca = {
    x: namespace.style.scaleX ? getNumFromString(namespace.style.scaleX) : 1,
    y: namespace.style.scaleY ? getNumFromString(namespace.style.scaleY) : 1,
  };

  if (transforms.length > 0) {
    const v = (transforms.map(transform => transformValueForProperty(transform[0], transform[1]).string));

    v.forEach((a) => {
      if (a.indexOf('translateX') >= 0 || a.indexOf('translate-x') >= 0) {
        trans.x = getNumFromString(a);
        trans.xU = getUnitFromString(a);
      } else if (a.indexOf('translateY') >= 0 || a.indexOf('translate-y') >= 0) {
        trans.y = getNumFromString(a);
        trans.yU = getUnitFromString(a);
      } else if (a.indexOf('translateZ') >= 0 || a.indexOf('translate-z') >= 0) {
        trans.z = getNumFromString(a);
        trans.zU = getUnitFromString(a);
      }

      if (a.indexOf('rotateX') >= 0 || a.indexOf('rotate-x') >= 0) {
        rot.x = getNumFromString(a);
      } else if (a.indexOf('rotateY') >= 0 || a.indexOf('rotate-y') >= 0) {
        rot.y = getNumFromString(a);
      } else if (a.indexOf('rotateZ') >= 0 || a.indexOf('rotate-z') >= 0) {
        rot.z = getNumFromString(a);
      } else if (a.indexOf('rotate') >= 0) {
        const sp = a.split(',');
        rot.x = getNumFromString(sp[0]);
        rot.y = getNumFromString(sp[1]);
        rot.z = getNumFromString(sp[2]);
      }

      if (a.indexOf('scaleX') >= 0 || a.indexOf('scale-x') >= 0) {
        sca.x = getNumFromString(a);
      } else if (a.indexOf('scaleY') >= 0 || a.indexOf('scale-y') >= 0) {
        sca.y = getNumFromString(a);
      }
      if (a.indexOf('skewX') >= 0 || a.indexOf('skew-x') >= 0) {
        ske.x = getNumFromString(a);
      } else if (a.indexOf('skewY') >= 0 || a.indexOf('skew-y') >= 0) {
        ske.y = getNumFromString(a);
      }
    });

    z[0] = `translate3d(${trans.x}${trans.xU},${trans.y}${trans.yU},${trans.z}${trans.zU})`;
    z[1] = `rotateX(${rot.x}${rot.u}) rotateY(${rot.y}${rot.u}) rotateZ(${rot.z}${rot.u})`;
    z[2] = `skew(${ske.x}${ske.u},${ske.y}${ske.u})`;
    z[3] = `scale(${sca.x},${sca.y})`;

    namespace.style['transform'] = z.join(' ');
  }
};

/**
 * applyStyle apply all the properties to the driver's namespace
 * 
 * @param {object} namespace 
 * @param {object} properties
 */
const applyStyle = (namespace, properties = namespace.style) => {
  index.check(properties, Object);
  const keys = Object.keys(properties);
  keys.forEach(k => namespace.style[k] = properties[k]);
  applyTransformProperties(namespace);
  
  return namespace.style;
};

/**
  * Add a global style object to the page
  * @param {object} namespace - driver's namespace
  * @param {function} callback - the callback to execute
  * @returns {Promise}
  */

const globalStyle = (namespace) => {
  applyStyle(namespace, namespace.style);
  namespace.global.id = namespace.global.id || uuid();
  const str = `.${namespace.global.id} {
    ${Object.keys(namespace.style).reduce((prev, key) => {
      return `${prev}
    ${key}: ${namespace.style[key]};`; 
    }, '').trim()}
}`;
  return str;
};

/**
 * 
 * Driver update method
 * trigger the update cycle
 * 
 * @param {Object} namespace 
 * @param {Object} mo 
 */
const update = function update(namespace, mo)  {
  if (namespace.$hooks) namespace.$hooks.filter(h => h.type === 'update').forEach(h => h.fn(namespace, mo));
};

const init = function init(namespace, ...args) {
  if (namespace.$hooks) namespace.$hooks.filter(h => h.type === 'init').forEach(h => h.fn(namespace, ...args));
};

const exportToJSON = ({
  style,
  global,
  $hooks,
}) => ({
  style,
  global,
  exportedHooks: $hooks.map(({ type, fn }) => ({
    type,
    fn: serialize(fn),
  })),
});

class CSSDriver {
  constructor(namespace) {
    this.id = 'CSS';
    this.namespace = namespace || {};
    this.namespace.style = this.namespace.style || {};
    this.namespace.global = this.namespace.global || { style: '' };
    this.namespace.$hooks = this.namespace.$hooks || []; 
    this.namespace.$hooks = this.namespace.$hooks.concat(this.namespace.exportedHooks ? this.namespace.exportedHooks.map(({ type, fn }) => ({
      type,
      fn: unserialize(fn),
    })) : []);
    delete this.namespace.exportedHooks;
    this.init();
  }
  init(...args) { return init.call(this, this.namespace, ...args); }
  update(...args) { return update.call(this, this.namespace, ...args); }
  applyStyle(style) { return applyStyle.call(this, this.namespace, style); }
  globalStyle() { return globalStyle.call(this, this.namespace); }
  exportToJSON() { return exportToJSON.call(this, this.namespace); }
}

export default CSSDriver;
export { transformProperties, transformValueForProperty, applyTransformProperties, applyStyle, globalStyle, update, init };
