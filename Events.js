import PubSub from 'pubsub-js';
import serialize from './libs/microutils/dist/functions/serialize';
import unserialize from './libs/microutils/dist/functions/unserialize';

export const createTopic = (namespace, topicName) => {
  const symbol = Symbol(topicName);
  namespace.topics[topicName] = {
    symbol,
    subscribers: [],
  };
};

export const subscribe = (namespace, topicName, fn) => {
  if (typeof fn !== 'function') throw new TypeError(`Cannot subscribe to ${topicName}. Provide a Function`);
  if (!namespace.topics[topicName]) createTopic(namespace, topicName);
  const token =  PubSub.subscribe(namespace.topics[topicName], fn);
  namespace.topics[topicName].subscribers.push({
    token,
    fn,
  });
  return token
};

export const unsubscribe = (namespace, token) => {
  const sub = namespace.topics.find(t => {
    const sb = t.subscribers.find(s => s.token === token);
    if (sb) {
      const sbIndex = t.subscribers.findIndex(s => s.token === token);
      if (sbIndex >= 0) t.subscribers.splice(sbIndex, 1);
    }
  });
  return PubSub.unsubscribe(token);
};

export const publish = (namespace, topicName, data) => PubSub.publish(namespace.topics[topicName], data);
export const publishSync = (namespace, topicName, data) => PubSub.publishSync(namespace.topics[topicName], data);

export const init = function init(namespace, ...args) {
  if (namespace.$hooks) namespace.$hooks.filter(h => h.type === 'init').forEach(h => h.fn(namespace, ...args));
};


/**
 *
 * Driver update method
 * trigger the update cycle
 *
 * @param {Object} namespace
 */
const update = (mo) => {};

const exportToJSON = ({
  topics,
  $hooks,
}) => ({
  exportedTopics: Object.keys(topics).reduce((acc, key) => {
    const topic = {
      label: key,
      subscribers: topics[key].subscribers.map(m => serialize(m.fn)),
    };
    acc.push(topic);
    return acc;
  }, []),
  exportedHooks: $hooks.map(({ type, fn }) => ({
    type,
    fn: serialize(fn),
  })),
});

export default class EventsDriver {
  constructor(namespace) {
    this.id = 'Events';
    this.namespace = namespace;
    this.namespace.topics = this.namespace.topics || {};
    this.namespace.$hooks = this.namespace.$hooks || []; 
    this.namespace.$hooks = this.namespace.$hooks.concat(namespace.exportedHooks ? namespace.exportedHooks.map(({ type, fn }) => ({
      type,
      fn: unserialize(fn),
    })) : []);
    delete this.namespace.exportedHooks;
    if (this.namespace.exportedTopics) {
      this.namespace.exportedTopics.forEach((topic) => {
        createTopic(this.namespace, topic.label);
        topic.subscribers.forEach(sub => subscribe(this.namespace, topic.label, unserialize(sub)));
      });
      delete this.namespace.exportedTopics;
    }
    this.init();
  }
  init(...args) { return init.call(this, this.namespace, ...args); }
  createTopic(topicName) { return createTopic.call(this, this.namespace, topicName); }
  subscribe(topicName, fn) { return subscribe.call(this, this.namespace, topicName, fn); }
  publish(topicName, data) { return publish.call(this, this.namespace, topicName, data); }
  publishSync(topicName, data) { return publishSync.call(this, this.namespace, topicName, data); }
  update(mo) { return update.call(this, mo); }
  exportToJSON() { return exportToJSON.call(this, this.namespace); }
}
