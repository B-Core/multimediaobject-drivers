import EventsDriverConstructor from './Events';

jest.useFakeTimers();

let EVENTS_NAMESPACE = {};
const EventsDriver = new EventsDriverConstructor(EVENTS_NAMESPACE);

describe('EventsDriver', () => {
  afterEach(() => {
    EVENTS_NAMESPACE = {};
  });
  test('should have an id of Events', () => {
    expect(EventsDriver.id).toEqual('Events');
  });

  test('should create a topic', () => {
    const driver = new EventsDriverConstructor(EVENTS_NAMESPACE);

    expect(driver).toBeInstanceOf(EventsDriverConstructor);
    expect(driver.namespace.topics).toBeDefined();

    expect(() => driver.createTopic('test')).not.toThrow();
    expect(driver.namespace.topics.test).toBeDefined();
    expect(typeof driver.namespace.topics.test).toEqual('object');
    expect(driver.namespace.topics.test).toHaveProperty('symbol');
    expect(driver.namespace.topics.test).toHaveProperty('subscribers', []);
  });

  test('should subscribe to an existing topic', () => {
    const driver = new EventsDriverConstructor(EVENTS_NAMESPACE);
    const mockFn = jest.fn();
    const subscribeMock = jest.fn(PubSub.subscribe);
    PubSub.subscribe = subscribeMock;
    driver.createTopic('test');

    expect(() => driver.subscribe('test', mockFn)).not.toThrow();
    expect(subscribeMock).toHaveBeenCalledWith(driver.namespace.topics.test, mockFn);

    subscribeMock.mockRestore();
  });

  test('should create the topic if it doesn\'t exist', () => {
    const driver = new EventsDriverConstructor(EVENTS_NAMESPACE);
    const mockFn = jest.fn();
    expect(() => driver.subscribe('test', mockFn)).not.toThrow();
    expect(driver.namespace.topics['test']).toBeDefined();
  });

  test('should throw an error if subscribinf with not a function', () => {
    const driver = new EventsDriverConstructor(EVENTS_NAMESPACE);
    expect(() => driver.subscribe('test', 'test')).toThrow(TypeError('Cannot subscribe to test. Provide a Function'));
  });

  test('should publishSync to a topic', () => {
    const driver = new EventsDriverConstructor(EVENTS_NAMESPACE);
    const mockFn = jest.fn();
    const dummy = {
      test: 10,
      lol: {
        nested: [],
      },
    };

    const testToken = driver.subscribe('test', mockFn);
    expect(testToken).toBeDefined();
    driver.publishSync('test', dummy);

    expect(mockFn.mock.calls[0][0].toString()).toEqual(driver.namespace.topics['test'].toString());
    expect(mockFn.mock.calls[0][1]).toEqual(dummy);
  });

  test('should publish to a topic', async () => {
    const driver = new EventsDriverConstructor(EVENTS_NAMESPACE);
    const mockFn = jest.fn();
    const dummy = {
      test: 10,
      lol: {
        nested: [],
      },
    };

    const testToken = driver.subscribe('test', mockFn);
    expect(testToken).toBeDefined();
    await driver.publish('test', dummy);

    expect(setTimeout).toHaveBeenLastCalledWith(expect.any(Function), 0);
  });

  test('should return topics as json export',() => {
    const originalConsoLog = global.console.log;
    const mockLog = jest.fn();
    global.console.log = mockLog;
    let test = false;
    const hooks = [
      {
        type: 'init',
        fn: function initHook(namespace, mo) {
          namespace.test = true;
        }
      } 
    ];

    const driver = new EventsDriverConstructor({
      $hooks: hooks,
      ...EVENTS_NAMESPACE,
    });
    driver.createTopic('test');
    driver.createTopic('test2');
    driver.createTopic('test3');
    driver.subscribe('test', function testFunction(test) { 
      test = true;
      console.log(test);
    });
    expect(driver.exportToJSON()).toEqual({
      exportedHooks: [{
        type: 'init',
        fn: {
          args: ['namespace',' mo'],
          body: `
        namespace.test = true;
      `,
        }
      }], 
      "exportedTopics": [
        {"label": "test", "subscribers": [
          { args: ["test"], body: `
      test = true;
      console.log(test);
    ` },
        ]},
        {"label": "test2", "subscribers": []},
        {"label": "test3", "subscribers": []
      }]});

    const driver2 = new EventsDriverConstructor(driver.exportToJSON());

    driver2.publishSync('test', test);
    expect(mockLog).toHaveBeenCalledWith(true);
    global.console.log = originalConsoLog;
  });

  test('should update', () => {
    const driver = new EventsDriverConstructor(EVENTS_NAMESPACE);

    expect(() => driver.update()).not.toThrow();
  });
});
