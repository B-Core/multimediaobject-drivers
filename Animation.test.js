import anime from 'animejs';
import AnimationDriver from './Animation';

describe('drivers/Animation', () => {
  let driver = null;
  let NAMESPACE = null;

  let target = null;

  beforeEach(() => {
    target = {};
    NAMESPACE = {
      targets: [target],
    };

    driver = new AnimationDriver(NAMESPACE);
  });

  test('hooks', () => {
    const hookMock = jest.fn();
    const nonHook = jest.fn();
    
    const hooks = {
      $hooks: [
        {
          type: 'update',
          fn: nonHook,
        },
      ],
    };
    const ADriver = new AnimationDriver(hooks);
    expect(nonHook).not.toHaveBeenCalled();

    ADriver.update();
    expect(nonHook).toHaveBeenCalledWith(undefined, hooks);
  });

  test('should start all values at default', () => {
    const otherNamespace = {};
    const d = new AnimationDriver(otherNamespace);
    expect(otherNamespace.currentAnimationName).toBeDefined();
    expect(otherNamespace.currentAnimation).toBeDefined();
    expect(otherNamespace.currentAnimation).toBeDefined();
    expect(otherNamespace.totalTime).toBeDefined();
    expect(otherNamespace.progress).toEqual(0);
    expect(otherNamespace.targets).toBeDefined();
    expect(otherNamespace.instance).toBeDefined();
  });

  test('.updateInstance should create a new instance with the currentAnimation', () => {
    const animation = {
      translateX: [
        { value: 100, duration: 1200 },
        { value: 0, duration: 800 },
      ],
    };

    NAMESPACE.animations = animation;
    driver.updateInstance();

    expect(NAMESPACE.instance.animatables).toHaveLength(1);
  });

  test('.updateInstance should instanciate a timeline if there are childs', () => {
    const animation = {
      translateX: [
        { value: 100, duration: 1200 },
      ],
    };
    const c = {
      targets: {
        translateX: 0,
      },
      currentAnimation: {
        translateX: [
          { value: 100, duration: 1200 },
          { value: 0, duration: 800 },
        ],
      },
      childs: [
        {
          targets: {
            translateX: 0,
          },
          currentAnimation: {
            translateX: [
              { value: 100, duration: 1200 },
              { value: 0, duration: 800 },
            ],
          },
        },
      ],
    };
    const childs = [
      c,
      c,
    ];

    NAMESPACE.targets = {
      translateX: 0,
    };
    NAMESPACE.currentAnimation = animation;
    NAMESPACE.childs = childs;
    driver.updateInstance();

    expect(NAMESPACE.timeline.duration).toEqual(2000);
    expect(NAMESPACE.timeline.children).toHaveLength(4);
    expect(JSON.stringify(NAMESPACE.childs[0].instance)).toEqual(JSON.stringify(NAMESPACE.timeline.children[2]));
    expect(JSON.stringify(NAMESPACE.childs[0].childs[0].instance)).toEqual(JSON.stringify(NAMESPACE.timeline.children[3]));

    expect(NAMESPACE.timeline.children[0].animations[0].tweens).toHaveLength(2);
    NAMESPACE.timeline.children.slice(1).forEach(ch => ch.animations.forEach(a => expect(a.tweens).toHaveLength(2)));
  });

  test('.play should play the animation', async () => {
    const animation = {
      translateX: [
        { value: 100, duration: 100 },
        { value: 0, duration: 200 },
      ],
    };

    NAMESPACE.currentAnimation = animation;
    driver.updateInstance();

    driver.play();
    await driver.finished();
    expect(NAMESPACE.instance.began).toEqual(true);
  });
  test('.pause should pause the animation', async () => {
    const animation = {
      translateX: [
        { value: 100, duration: 100 },
        { value: 0, duration: 200 },
      ],
    };

    NAMESPACE.currentAnimation = animation;
    driver.updateInstance();

    driver.play();
    await driver.finished();
    driver.pause();
    expect(NAMESPACE.instance.began).toEqual(true);
    expect(NAMESPACE.instance.paused).toEqual(true);
  });

  test('.restart should restart the animation', async () => {
    const animation = {
      translateX: [
        { value: 100, duration: 100 },
        { value: 0, duration: 200 },
      ],
    };

    NAMESPACE.currentAnimation = animation;
    driver.updateInstance();

    driver.play();
    await driver.finished();
    driver.pause();
    expect(NAMESPACE.instance.began).toEqual(true);
    expect(NAMESPACE.instance.paused).toEqual(true);

    driver.restart();
  });

  test('.reverse should reverse the animation', async () => {
    const animation = {
      translateX: [
        { value: 100, duration: 10 },
      ],
    };

    NAMESPACE.currentAnimation = animation;
    driver.updateInstance();

    driver.play();
    await driver.finished();
    driver.pause();
    expect(NAMESPACE.instance.began).toEqual(true);
    expect(NAMESPACE.instance.paused).toEqual(true);

    driver.reverse();

    expect(NAMESPACE.instance.direction).toEqual('normal');
  });

  test('.seek should seek the animation', () => {
    const animation = {
      translateX: [
        { value: 100, duration: 1000 },
      ],
    };

    NAMESPACE.currentAnimation = animation;
    driver.updateInstance();

    driver.seek(500);

    expect(NAMESPACE.instance.progress).toEqual(50);
    expect(NAMESPACE.instance.currentTime).toEqual(500);
  });

  test('.update should update the animation', () => {
    const animation = {
      translateX: [
        { value: 100, duration: 1000 },
      ],
    };

    NAMESPACE.currentAnimation = animation;
    driver.updateInstance();

    driver.update();
  });

  test('.easings should return an easing list', () => {
    const easingProperties = ['easeInBack', 'easeInCirc', 'easeInCubic', 'easeInElastic', 'easeInExpo', 'easeInOutBack', 'easeInOutCirc', 'easeInOutCubic', 'easeInOutElastic', 'easeInOutExpo', 'easeInOutQuad', 'easeInOutQuart', 'easeInOutQuint', 'easeInOutSine', 'easeInQuad', 'easeInQuart', 'easeInQuint', 'easeInSine', 'easeOutBack', 'easeOutCirc', 'easeOutCubic', 'easeOutElastic', 'easeOutExpo', 'easeOutQuad', 'easeOutQuart', 'easeOutQuint', 'easeOutSine', 'linear'];

    for (const key of easingProperties) {
      expect(driver.namespace.easings).toHaveProperty(key);
    }
  });

  test('.changeAnimation should change the currentAnimation if it exists', () => {
    const animations = {
      test: {
        translateX: [
          { value: 100, duration: 1000 },
        ],
      },
    };

    NAMESPACE.animations = animations;

    expect(() => driver.changeAnimation('test')).not.toThrow();
    expect(NAMESPACE.currentAnimation).toEqual(animations.test);
    expect(NAMESPACE.currentAnimationName).toEqual('test');

    expect(() => driver.changeAnimation('lol')).toThrow(Error('animation lol doesn\'t exist'));
  });

  test('.deleteAnimation should an animation and change to default if it was selected', () => {
    const animations = {
      default: {
        width: [
          { value: '10%', duration: 1000 },
        ],
      },
      test: {
        translateX: [
          { value: 100, duration: 1000 },
        ],
      },
    };

    NAMESPACE.animations = animations;

    driver.changeAnimation('test');

    expect(() => driver.deleteAnimation('test')).not.toThrow();
    expect(NAMESPACE.animations.test).toBeUndefined();
    expect(NAMESPACE.currentAnimation).toEqual(animations.default);
  });

  test('.exportToJSON', () => {
    const hooks = [
      {
        type: 'init',
        fn: function initHook(namespace, mo) {
          namespace.test = true;
        }
      } 
    ];
    const driver = new AnimationDriver({
      $hooks: hooks,
      animations: {
        default: {
          translateX: [
            { value: 100, duration: 100 },
            { value: 0, duration: 200 },
          ],
        }
      },
    });
    
    const json = driver.exportToJSON();

    expect(json).toEqual({
      exportedHooks: [{
        type: 'init',
        fn: {
          args: ['namespace',' mo'],
          body: `
        namespace.test = true;
      `,
        }
      }], 
      totalTime: 10,
      animations: {
        default: {
          translateX: [
            { delay: 0, value: 100, duration: 100 },
            { delay: 0, value: 0, duration: 200 },
          ],
        }
      },
      currentAnimation: {
        translateX: [
          { delay: 0, value: 100, duration: 100 },
          { delay: 0, value: 0, duration: 200 },
        ],
      },
      currentAnimationName: 'default',
    });

    const driver2 = new AnimationDriver({
      $hooks: [
        {
          type: 'update',
          fn: function updateHook(mo, namespace) {
            namespace.testUpdate = true;
          }
        },
      ],
      ...json,
    });
    expect(driver2.namespace.$hooks).toHaveLength(2);
    expect(driver2.namespace.test).toBe(true);
    driver2.update({});
    expect(driver2.namespace.testUpdate).toBe(true);
  });

});
