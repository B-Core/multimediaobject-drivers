import serialize from './libs/microutils/dist/functions/serialize';
import unserialize from './libs/microutils/dist/functions/unserialize';

import anime from 'animejs';

const config = ({
  totalTime: 10,
});

const progress = (namespace, anim) => {
  namespace.progress = anim.progress;
};

const addChilds = (namespace, child) => {
  const el = child ? child : namespace;
  if (child) {
    namespace.timeline.add({
      targets: el.targets,
      ...el.currentAnimation,
      update: progress.bind(undefined, el),
      offset: 0,
    });
    el.instance = namespace.timeline.children[namespace.timeline.children.length <= 0 ? 0 : namespace.timeline.children.length - 1]; 
    el.instance = el.instance || namespace.instance;
    if (!el.instance.getValue) el.instance.getValue = anime.getValue;
  }
  if (el.childs) el.childs.forEach((c) => addChilds(namespace, c));
};

const cleanTimeline = (timeline) => {
  while (timeline.animations.length > 0) {
    const index = timeline.animations.length > 0 ? timeline.animations.length - 1 : 0;
    timeline.animations.splice(index, 1);
  }
  while (timeline.children.length > 0) {
    const index = timeline.children.length > 0 ? timeline.children.length - 1 : 0;
    timeline.children.splice(index, 1);
  }
}; 

const updateInstance = (namespace) => { 
  if (namespace.childs.length > 0 && !namespace.MOParent) {
    if (namespace.timeline) cleanTimeline(namespace.timeline);
    namespace.timeline = namespace.timeline || anime.timeline({
      loop: namespace.loop || false,
      delay: namespace.delay || 0,
      update: progress.bind(undefined, namespace),
      autoplay: namespace.autoplay || false,
    });
    if (!namespace.timeline.getValue) namespace.timeline.getValue = anime.getValue;
    addChilds(namespace);
  } else if (namespace.childs,length === 0 && !namespace.MOParent) {
    if (namespace.timeline) cleanTimeline(namespace.instance);
    namespace.instance = anime({
      targets: namespace.targets,
      ...namespace.currentAnimation,
      update: progress.bind(undefined, namespace),
      autoplay: namespace.autoplay || false,
    });
    namespace.instance.getValue = anime.getValue;
  }
};

const deleteAnimation = (namespace, animationName) => {
  delete namespace.animations[animationName];
  if (namespace.currentAnimationName === animationName) {
    namespace.currentAnimation = namespace.animations.default;
    namespace.currentAnimationName = 'default';
  }
};

const changeAnimation = (namespace, animationName) => {
  if (!namespace.animations[animationName]) throw new Error(`animation ${animationName} doesn't exist`);
  namespace.currentAnimationName = animationName;
  namespace.currentAnimation = namespace.animations[animationName];
};

const play = namespace => namespace.timeline ? namespace.timeline.play() : namespace.instance.play();
const pause = namespace => namespace.timeline ? namespace.timeline.pause() : namespace.instance.pause();
const restart = namespace => namespace.timeline ? namespace.timeline.restart() : namespace.instance.restart();
const reverse = namespace => namespace.timeline ? namespace.timeline.reverse() : namespace.instance.reverse();
const seek = (namespace, time) => namespace.timeline ? namespace.timeline.seek(time) : namespace.instance.seek(time);
const finished = namespace => namespace.timeline ? namespace.timeline.finished() : namespace.instance.finished;
/**
 * 
 * Driver update method
 * trigger the update cycle
 * 
 * @param {Object} namespace 
 * @param {Object} mo 
 */
const update = function update(namespace, mo)  {
  if (namespace.$hooks) namespace.$hooks.filter(h => h.type === 'update').forEach(h => h.fn(mo, namespace));
};

export const init = function init(namespace, ...args) {
  namespace.animations = namespace.animations || {};
  namespace.currentAnimationName = namespace.currentAnimationName || 'default';
  namespace.currentAnimation = namespace.currentAnimation || namespace.animations[namespace.currentAnimationName] || {};
  namespace.totalTime = namespace.totalTime || config.totalTime;
  namespace.progress = 0;
  namespace.targets = namespace.targets || [];
  namespace.autoplay = namespace.autoplay || false;

  namespace.easings = anime.easings;

  namespace.childs = namespace.childs || [];
  if (namespace.$hooks) namespace.$hooks.filter(h => h.type === 'init').forEach(h => h.fn(namespace, ...args));
}

const exportToJSON = ({
  animations,
  currentAnimationName,
  currentAnimation,
  totalTime,
  $hooks,
}) => ({
  animations,
  currentAnimationName,
  currentAnimation,
  totalTime,
  exportedHooks: $hooks.map(({ type, fn }) => ({
    type,
    fn: serialize(fn),
  })),

});

export default class AnimationDriver {
  constructor(namespace) {
    this.id = 'Animation';
    this.namespace = namespace;
    this.namespace.$hooks = this.namespace.$hooks || []; 
    this.namespace.$hooks = this.namespace.$hooks.concat(this.namespace.exportedHooks ? this.namespace.exportedHooks.map(({ type, fn }) => ({
      type,
      fn: unserialize(fn),
    })) : []);
    delete this.namespace.exportedHooks;
    this.init();
    this.updateInstance();
  }

  init(...args) { return init.call(this, this.namespace, ...args); }
  updateInstance() { return updateInstance.call(this, this.namespace); }
  changeAnimation(animationName) { return changeAnimation.call(this, this.namespace, animationName); }
  deleteAnimation(animationName) { return deleteAnimation.call(this, this.namespace, animationName); }
  update(mo) { return update.call(this, this.namespace, mo); }
  play() { return play.call(this, this.namespace); }
  pause() { return pause.call(this, this.namespace); }
  restart() { return restart.call(this, this.namespace); }
  reverse() { return reverse.call(this, this.namespace); }
  seek(time) { return seek.call(this, this.namespace, time); }
  finished() { return finished.call(this, this.namespace); }
  exportToJSON() { return exportToJSON.call(this, this.namespace); }
}
export { progress, updateInstance, play, pause, restart, reverse, seek, finished, update };
